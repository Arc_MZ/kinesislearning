# Learning Amazon Kinesis Development, Part One: Streams, Producers, and Consumers

The learning-module-1 branch provides the code for the first learning module, [Streams, Producers and Consumers][learning-kinesis-part-1], in the [Learning Kinesis Development][learning-kinesis] series.

[learning-kinesis]: http://docs.aws.amazon.com/kinesis/latest/dev/learning-kinesis.html
[learning-kinesis-part-1]: http://docs.aws.amazon.com/kinesis/latest/dev/learning-kinesis-module-one.html

## Amazon Kinesis Data Streams Developer Guide
https://docs.aws.amazon.com/streams/latest/dev/building-producers.html

## KCL client library
https://docs.aws.amazon.com/streams/latest/dev/developing-consumers-with-kcl.html

## Update Shard Count
https://docs.aws.amazon.com/kinesis/latest/APIReference/API_UpdateShardCount.html

## json format
{
  "tickerSymbol": "AMZN", 
  "tradeType": "BUY", 
  "price": 395.87,
  "quantity": 16, 
  "id": 3567129045
}

## result
    
    StockTradesWriter
    
        Nov. 03, 2018 6:12:45 PM com.amazonaws.services.kinesis.samples.stocktrades.writer.StockTradesWriter sendStockTrade
        INFO: Putting trade: ID 326: SELL 6864 shares of CHL for $72.16
        
        Nov. 03, 2018 6:12:46 PM com.amazonaws.services.kinesis.samples.stocktrades.writer.StockTradesWriter sendStockTrade
        INFO: Putting trade: ID 327: BUY 6209 shares of MSFT for $44.15
        
        Nov. 03, 2018 6:12:47 PM com.amazonaws.services.kinesis.samples.stocktrades.writer.StockTradesWriter sendStockTrade
        INFO: Putting trade: ID 328: BUY 6702 shares of NVS for $120.47

    
    StockTradesProcessor
        
        Nov. 03, 2018 6:10:14 PM com.amazonaws.services.kinesis.samples.stocktrades.processor.StockTradeRecordProcessor initialize
        INFO: Initializing record processor for shard: shardId-000000000003
        Nov. 03, 2018 6:11:14 PM com.amazonaws.services.kinesis.samples.stocktrades.processor.StockTradeRecordProcessor checkpoint
        ****** Shard shardId-000000000003 stats for last 1 minute ******
        INFO: Checkpointing shard shardId-000000000003
        Most popular stock being bought: WMT, 7 buys.
        Most popular stock being sold: NVS, 6 sells.
        ****************************************************************
       
    
